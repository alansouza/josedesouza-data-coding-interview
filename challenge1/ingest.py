from pyspark.sql import Row, DataFrame
from pyspark.sql import SparkSession
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--csv_path', dest='csv_path', type=str)
parser.add_argument('--database', dest='database', type=str)
parser.add_argument('--table', dest='table', type=str)
args = parser.parse_args()


spark = SparkSession \
    .builder \
    .appName("Python Spark SQL basic example") \
    .config("spark.jars","spark/jdbc/postgresql-42.6.0.jar") \
    .getOrCreate()

def read_table(database: str, table: str):
    df = spark.read \
    .format("jdbc") \
    .option("url", f"jdbc:postgresql://localhost:5432/{database}") \
    .option("dbtable", table) \
    .option("user", "postgres") \
    .option("password", "Password1234**") \
    .option("driver", "org.postgresql.Driver") \
    .load()  
    return df

def read_csv(path: str):
    df = spark.read.option("header", "true").csv(path)
    return df

def write_table(dataframe: DataFrame, database: str, table: str):
    dataframe.write \
    .format("jdbc") \
    .option("url", f"jdbc:postgresql://localhost:5432/{database}") \
    .option("dbtable", table) \
    .option("user", "postgres") \
    .option("password", "Password1234**") \
    .option("driver", "org.postgresql.Driver") \
    .mode("append") \
    .save()


df_csv = read_csv(args.csv_path)

write_table(df_csv, args.database, args.table)






